import 'dart:async';

import 'package:dapp/utils/delayed_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class ChatIn extends StatefulWidget {
  ChatIn({Key key}) : super(key: key);

  @override
  _ChatInState createState() => _ChatInState();
}

class _ChatInState extends State<ChatIn> with SingleTickerProviderStateMixin {
  TextEditingController _controller = new TextEditingController();
  FocusNode inputFieldNode;
  final String _messageTypeSending = "1";
  final String _messageTypeReceiving = "2";
  final List<Map<String, String>> _messages = <Map<String, String>>[];
  final ScrollController _messageListController = ScrollController();

  @override
  void initState() {
    super.initState();
    inputFieldNode = FocusNode();
  }

  @override
  void dispose() {
    _controller.dispose();
    inputFieldNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 10,
        title: Text(
          "Berk, 22",
          style: TextStyle(fontSize: 15),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(Ionicons.ios_arrow_back),
          color: Colors.white.withOpacity(1),
        ),
      ),
      body: Container(
        color: Colors.black,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: messageListBuilder(context),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Form(
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[900].withOpacity(0.5),
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        flex: 8,
                        child: TextField(
                          controller: this._controller,
                          onSubmitted: (result) {
                            this._controller.clear();
                            FocusScope.of(context).requestFocus(inputFieldNode);
                            if (result.length > 0) {
                              setState(() {
                                Map<String, String> map = {
                                  result: this._messageTypeSending
                                };
                                this._messages.add(map);
                                this._messageListController.animateTo(
                                    this
                                            ._messageListController
                                            .position
                                            .maxScrollExtent +
                                        100,
                                    duration: Duration(milliseconds: 1000),
                                    curve: Curves.linearToEaseOut);
                              });
                            }
                          },
                          minLines: 1,
                          focusNode: inputFieldNode,
                          maxLines: 3,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.send,
                          style: TextStyle(color: Colors.white60),
                          decoration: InputDecoration(
                            hintStyle:
                                TextStyle(fontSize: 12, color: Colors.grey),
                            hintText: 'Bir şeyler yaz ..',
                            prefixIcon: Icon(FontAwesome5.smile_beam,
                                color: Colors.grey),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(20),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child: Row(
                          children: <Widget>[
                            InkWell(
                                onTap: () {
                                  String result = this._controller.text;
                                  this._controller.clear();
                                  if (result.length > 0) {
                                    setState(() {
                                      Map<String, String> map = {
                                        result: this._messageTypeSending
                                      };
                                      this._messages.add(map);
                                      this._messageListController.animateTo(
                                          this
                                                  ._messageListController
                                                  .position
                                                  .maxScrollExtent +
                                              100,
                                          duration:
                                              Duration(milliseconds: 1000),
                                          curve: Curves.linearToEaseOut);
                                    });
                                  }
                                },
                                child:
                                    Icon(Icons.send, color: Colors.blueGrey)),
                            SizedBox(
                              width: 5,
                            ),
                            InkWell(
                                child: Icon(Icons.gif, color: Colors.blueGrey))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ListView messageListBuilder(BuildContext context) {
    return ListView.builder(
      controller: this._messageListController,
      itemCount: this._messages.length,
      itemBuilder: (context, index) {
        return DelayedAnimation(
            curve: Curves.fastLinearToSlowEaseIn,
            delay: 1,
            child: messageBuilder(context, message: this._messages[index]));
      },
    );
  }

  Row messageBuilder(BuildContext context,
      {@required Map<String, String> message}) {
    if (message.values.first == this._messageTypeSending)
      return buildOnRightSide(context, message.keys.first);
    else if (message.values.first == this._messageTypeReceiving)
      return buildOnLeftSide(context, message.keys.first);
    return null;
  }

  Row buildOnRightSide(BuildContext context, String message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: MediaQuery.of(context).size.width / 12),
        CircleAvatar(
            minRadius: 10,
            maxRadius: 20,
            backgroundImage: AssetImage("assets/e4.png")),
        SizedBox(width: MediaQuery.of(context).size.width / 12),
        Flexible(
          child: Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(top: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(message,
                          style: GoogleFonts.blinker(
                            textStyle: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.normal,
                                color: Colors.white70),
                          )),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  DateTime.now().hour.toString() +
                      ":" +
                      DateTime.now().minute.toString(),
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.white30),
                ),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15)),
                gradient: LinearGradient(
                    begin: Alignment.bottomRight,
                    end: Alignment.topLeft,
                    colors: [
                      Color(0xFF0f0211),
                      Colors.grey[900].withOpacity(0.5)
                    ])),
          ),
        )
      ],
    );
  }

  Row buildOnLeftSide(BuildContext context, String message) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(top: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        message,
                        style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.normal,
                            color: Colors.white70),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '17.05',
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.white30),
                ),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(15)),
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [
                      Color(0xFF0f0211),
                      Colors.white.withOpacity(0.2)
                    ])),
          ),
        ),
        SizedBox(width: MediaQuery.of(context).size.width / 12),
        CircleAvatar(
            minRadius: 10,
            maxRadius: 20,
            backgroundImage: AssetImage("assets/ct2.png")),
        SizedBox(width: MediaQuery.of(context).size.width / 12),
      ],
    );
  }

  Row trash() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Icon(
              Icons.photo_camera,
              color: Colors.deepPurple,
            ),
            SizedBox(width: 10),
            Icon(
              Icons.mic_none,
              color: Colors.deepPurple,
            ),
          ],
        )
      ],
    );
  }
}
